## Interface: 90001
## Title: Guild Helper
## Author: Vaynt
## Notes: Helper addon for guild needs.
## Version 0.0.1
## SavedVariablesPerCharacter: GuildHelperConfig

widgets\widgets.lua
widgets\checkbutton.lua

config.lua
main.lua