local addon_name, ns = ...
local widgets = ns.widgets
ns.config = {}
local config = ns.config
local temp_config = {}
local check_buttons = {}

function config:create_config()
    local function save_config()
        for k, v in pairs(temp_config) do
            GuildHelperConfig[k] = v
        end

        temp_config = {}
    end

    local function cancel_config()
        temp_config = {}
    end

    local function refresh_config()
        for k, v in pairs(check_buttons) do
            v:SetChecked(GuildHelperConfig[k])
        end
    end

    -- Create the default Interface > AddOns frame.
    local guild_helper_config_frame = CreateFrame("Frame", "guild_helper_config_frame", UIParent)

    -- Name is required.
    guild_helper_config_frame.name = "Guild Helper"
    guild_helper_config_frame.okay = save_config
    guild_helper_config_frame.cancel = cancel_config
    guild_helper_config_frame.refresh = refresh_config

    -- Add it.
    InterfaceOptions_AddCategory(guild_helper_config_frame)

    local function congratulate_guild_achievement_on_click()
        if temp_config.congratulate_guild_achievement == nil then
            temp_config.congratulate_guild_achievement = not GuildHelperConfig.congratulate_guild_achievement
        else
            temp_config.congratulate_guild_achievement = not temp_config.congratulate_guild_achievement
        end
    end

    -- Congratule Guild Achievement Check Button.
    local congratulate_guild_achievement_check_button = widgets:create_checkbutton(guild_helper_config_frame, "TOPLEFT", 10, -10, "Congratulate Guild Achievements", "Enable this option to automatically congratulate guild members on their achievements.", congratulate_guild_achievement_on_click)
    congratulate_guild_achievement_check_button:SetChecked(GuildHelperConfig.congratulate_guild_achievement)
    check_buttons.congratulate_guild_achievement = congratulate_guild_achievement_check_button
end