local addon_name, ns = ...
local widgets = ns.widgets
local config = ns.config

ns.addon_loaded = false

-- Create a frame.
local guild_helper_event_frame = CreateFrame("Frame")

guild_helper_event_frame:RegisterEvent("ADDON_LOADED")
-- Register event for guild member achievement.
guild_helper_event_frame:RegisterEvent("CHAT_MSG_GUILD_ACHIEVEMENT")

-- Set Script for OnEvent.
guild_helper_event_frame:SetScript("OnEvent",
        function(self, event, ...)
            if event == "ADDON_LOADED" then
                local addon = ...

                if addon == "GuildHelper" then
                    if GuildHelperConfig == nil then
                        GuildHelperConfig = {}
                        GuildHelperConfig.congratulate_guild_achievement = false
                    else
                        if GuildHelperConfig.congratulate_guild_achievement == nil then
                            GuildHelperConfig.congratulate_guild_achievement = false
                        end
                    end

                    ns.addon_loaded = true
                    config:create_config()
                end
            end

            if ns.addon_loaded then
                -- Guild Member Achievement event.
                if event == "CHAT_MSG_GUILD_ACHIEVEMENT" then
                    if GuildHelperConfig.congratulate_guild_achievement then
                        local text, player_name, language_name, channel_name, player_name_2, special_flags, zone_channel_id, channel_index, channel_base_name, arg10, chat_line_id, sender_guid, is_mobile, is_subtitle, hide_sender_in_letterbox, suppress_raid_icons = ...
                        local current_player_guid = UnitGUID("player")

                        if sender_guid ~= current_player_guid then
                            local localized_class, english_class, localized_race, english_race, gender, name, server = GetPlayerInfoByGUID(sender_guid)

                            local congrats_messages = {
                                "Congratulations " .. name .. " on your achievement!",
                                "Congratz " .. name .. "!",
                                "Gratz!",
                                "congratz",
                                "gratz",
                                "gz",
                                "gz " .. name,
                                "gratz " .. name,
                                "Congratz " .. name,
                                "Well done " .. name .. "!"
                            }

                            SendChatMessage(congrats_messages[math.random(#congrats_messages)], "GUILD")
                        end
                    end
                end
            end
 end)