local addon_name, ns = ...
local widgets = ns.widgets

local count = 0;

function widgets:create_checkbutton(parent, point, x, y, text, tooltip, on_click)
    local checkbutton = CreateFrame("CheckButton", "guild_helper_check_button_" .. count, parent, "ChatConfigCheckButtonTemplate")
    checkbutton:SetPoint(point, x, y)
    getglobal(checkbutton:GetName() .. 'Text'):SetText(text)
    checkbutton.tooltip = tooltip
    checkbutton:SetScript("OnClick", on_click)

    count = count + 1

    return checkbutton
end